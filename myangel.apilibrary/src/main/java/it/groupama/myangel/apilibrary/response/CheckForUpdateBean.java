package it.groupama.myangel.apilibrary.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Francesco in 15/06/17.
 */

public class CheckForUpdateBean implements Serializable {

    private boolean active;
    private boolean force_update;
    private String message;
    @SerializedName("store_url")
    private String storeUrl;
    @SerializedName("target_version")
    private String targetVersion;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isForce_update() {
        return force_update;
    }

    public void setForce_update(boolean force_update) {
        this.force_update = force_update;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStoreUrl() {
        return storeUrl;
    }

    public void setStoreUrl(String storeUrl) {
        this.storeUrl = storeUrl;
    }

    public String getTargetVersion() {
        return targetVersion;
    }

    public void setTargetVersion(String targetVersion) {
        this.targetVersion = targetVersion;
    }
}
