package it.groupama.myangel.apilibrary.exeception;

/**
 * Created by Francesco in 20/07/17.
 */

public class IncorrectPropertyException extends RuntimeException {

    public IncorrectPropertyException() {
    }

    public IncorrectPropertyException(String message) {
        super(message);
    }

    public IncorrectPropertyException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectPropertyException(Throwable cause) {
        super(cause);
    }
}
