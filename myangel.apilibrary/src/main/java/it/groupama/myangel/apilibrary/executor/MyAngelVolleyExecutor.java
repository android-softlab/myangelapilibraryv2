package it.groupama.myangel.apilibrary.executor;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;


import it.groupama.myangel.apilibrary.exeception.InternetPermissionDeniedException;
import it.groupama.myangel.apilibrary.exeception.MissingPropertyException;
import it.groupama.myangel.apilibrary.executor.crypt.CryptKey;
import it.groupama.myangel.apilibrary.executor.crypt.CryptManagerFactory;
import it.groupama.myangel.apilibrary.executor.crypt.exception.NoHandShakeException;
import it.groupama.myangel.apilibrary.request.MyAngelRequest;
import it.groupama.myangel.apilibrary.response.MyAngelResponse;
import it.groupama.myangel.apilibrary.token.RequestTokenGenerator;
import it.groupama.myangel.apilibrary.util.UrlUtils;
import it.groupama.myangel.utilities.java.ArraySetList;
import it.groupama.myangel.utilities.java.MapStringObject;
import it.groupama.myangel.utilities.logger.Logger;
import it.groupama.security.crypt.MemorySecurityCryptManager;
import it.groupama.security.crypt.exception.CryptCheckException;

import static com.android.volley.DefaultRetryPolicy.DEFAULT_BACKOFF_MULT;
import static it.groupama.myangel.apilibrary.executor.MyAngelVolleyExecutorOptions.OPTION_RETRY;
import static it.groupama.myangel.apilibrary.executor.MyAngelVolleyExecutorOptions.OPTION_SPINNER_SHOW;
import static it.groupama.myangel.apilibrary.executor.MyAngelVolleyExecutorOptions.OPTION_TIMEOUT;


/**
 * Created by Francesco in 27/06/17.
 */

@SuppressWarnings("StringBufferReplaceableByString")
public class MyAngelVolleyExecutor<SR> {

    private static final String HEADER_REQUEST_TOKEN = "Request-Token";
    private static final String HEADER_BASIC_AUTHENTICATION = "Authorization";

    public static final String CRYPT_HEADER_KEY_EXCHANGESERVICE_PUBLIC_KEY = "public-key";
    public static final String CRYPT_HEADER_KEY_EXCHANGESERVICE_SERVICE_NAME = "service-name";

    public static final String CRYPT_HEADER_KEY_DECRYPTER_KEY = "mya-key";
    public static final String CRYPT_HEADER_KEY_UUID = "uuid";

    public static final String CRYPT_HEADER_KEY_BYPASS_KEY = "bypass-key";

    public static final String CRYPT_REQUEST_PARAM_BODY = "body";

    public static final String CRYPT_HEADER_KEY_USER_ID = "userid";

    public static final String CRYPT_HEADER_KEY_BUNDLE_ID = "bundleId";


    protected final Logger logger = Logger.getIntance(getClass());

    protected final MemorySecurityCryptManager cryptManager;

    protected final Gson gson = new GsonBuilder().create();

    protected final MyAngelVolleyExecutorOptions options = new MyAngelVolleyExecutorOptions();


    protected Method method;
    protected String service;
    protected MyAngelRequest requestBody;
    private Object serviceBody;

    protected Map<String, String> headers = new LinkedHashMap<String, String>() {{
        put("Content-Type", "application/json");
    }};

    protected final List<Class<? extends Exception>> noStopSpinner = new ArraySetList<>();

    protected Map<String, String> queryParameters = new LinkedHashMap<>();


    private SuccessListener successListener;
    private ErrorListener errorListener;


    private Class<SR> serviceResponseBodyClass;
    private MyAngeSuccessListener<SR> srSuccessListener;
    private MyAngeErrorListener<SR> srErrorListener;


    private int responseStatusCode = -1;
    private Map<String, String> responseHeaders;


    private AbstractSpinnerProcessor spinnerProcessor = new DefaultSpinnerProcessor();
    private AbstractErrorMessageProcessor errorMessageProcessor = new DefaultErrorMessageProcessor(null);
    protected final Context context;


    MyAngelVolleyExecutor(Context context) {
        this.context = context;

        cryptManager = CryptManagerFactory.getInstance();

        final MyAngelVolleyExecutorOptions.ErrorMessageDialogData errorDialogData = options.getMessageErrorType();
        setErrorMessageType(errorDialogData.getErrorType(), errorDialogData.getData());


        init();
    }

    protected void init() {

    }

    private void initSpinner() {
        boolean showSpinner = options.isShowSpinner();
        showSpinner(showSpinner);

        if (showSpinner && !spinnerProcessor.isShowing()) {
            ProgressDialog customProgressDialog = options.getCustomProgressDialog(context);

            if (customProgressDialog != null) {
                spinnerProcessor.setProgressDialog(customProgressDialog);
            } else {
                spinnerProcessor.init(context);
            }

            String message = options.getSpinnerMessage();
            if (!TextUtils.isEmpty(message)) {
                spinnerMessage(message);
            }
        }

    }

    public MyAngelVolleyExecutor<SR> service(String service) {
        this.service = service;
        return this;
    }

    public MyAngelVolleyExecutor<SR> specificUrlFree(String url) {
        options.setSpecificOptions(MyAngelVolleyExecutorOptions.OPTION_URL_FREE, url);
        return this;
    }

    public MyAngelVolleyExecutor<SR> specificUrlAuth(String url) {
        options.setSpecificOptions(MyAngelVolleyExecutorOptions.OPTION_URL_AUTH, url);
        return this;
    }

    /**
     * {@link com.android.volley.Request.Method} value is equal at ordinal of {@link Method}
     *
     * @param method {@link com.android.volley.Request.Method}
     * @return
     */
    public MyAngelVolleyExecutor<SR> method(int method) {
        this.method = Method.valueOf(method);
        return this;
    }

    /**
     *
     *
     * @param method
     * @return
     */
    public MyAngelVolleyExecutor<SR> method(Method method) {
        this.method = method;
        return this;
    }

    public MyAngelVolleyExecutor<SR> headers(Map<String, String> headers) {
        this.headers.putAll(headers);
        return this;
    }

    public MyAngelVolleyExecutor<SR> header(String headerKey, String headerValue) {
        this.headers.put(headerKey, headerValue);
        return this;
    }

    public MyAngelVolleyExecutor<SR> basicAuthentication(final String basicAuthentication) {
        this.headers.put(HEADER_BASIC_AUTHENTICATION, basicAuthentication);
        return this;
    }

    public MyAngelVolleyExecutor<SR> queryParameters(Map<String, String> headers) {
        this.queryParameters.putAll(headers);
        return this;
    }

    public MyAngelVolleyExecutor<SR> queryParameter(String headerKey, String headerValue) {
        this.queryParameters.put(headerKey, headerValue);
        return this;
    }

    public MyAngelVolleyExecutor<SR> body(MyAngelRequest requestBody) {
        this.requestBody = requestBody;
        return this;
    }

    public MyAngelRequest getRequestBody() {
        return requestBody;
    }

    public MyAngelVolleyExecutor<SR> serviceBody(Object serviceBody) {
        this.serviceBody = serviceBody;

//        if (this.requestBody == null) {
//            this.requestBody = new MyAngelRequest();
//        }

//        this.requestBody.addBodyParameters(serviceBody);

        return this;
    }

//    public MyAngelVolleyExecutor<R> responseClass(Class<R> responseBodyClass) {
//        this.responseBodyClass = responseBodyClass;
//        return this;
//    }

    public MyAngelVolleyExecutor<SR> success(SuccessListener successListener) {
        this.successListener = successListener;
        return this;
    }

    public MyAngelVolleyExecutor<SR> error(ErrorListener errorListener) {
        this.errorListener = errorListener;
        return this;
    }

    public MyAngelVolleyExecutor<SR> serviceResponseClass(Class<SR> serviceResponseBodyClass) {
        this.serviceResponseBodyClass = serviceResponseBodyClass;
        return this;
    }

    public MyAngelVolleyExecutor<SR> success(MyAngeSuccessListener<SR> successListener) {
        this.srSuccessListener = successListener;
        return this;
    }

    public MyAngelVolleyExecutor<SR> error(MyAngeErrorListener<SR> errorListener) {
        this.srErrorListener = errorListener;
        return this;
    }

//    public MyAngelVolleyExecutor<SR> showSpinner(final boolean show) {
//        String message = spinnerProcessor.message;
//
//        if (show) {
//            spinnerProcessor = new DefaultSpinnerProcessor();
//        } else { // Potrebbe essere inutile
//            spinnerProcessor = new NoSpinnerProcessor();
//        }
//
//        spinnerProcessor.message = message;
//
//        return this;
//    }

//    public MyAngelVolleyExecutor<SR> setSpinner(final ProgressDialog progressDialog) {
//        setSpinnerType(SpinnerMessageType.CUSTOM, progressDialog);
//        return this;
//    }

    public MyAngelVolleyExecutor<SR> showSpinner(final boolean showSpinner) {
        options.setSpecificOptions(OPTION_SPINNER_SHOW, showSpinner);
        if (showSpinner) {
            spinnerProcessor = new DefaultSpinnerProcessor();
        } else {
            spinnerProcessor = new NoSpinnerProcessor();
        }

        return this;
    }

    public MyAngelVolleyExecutor<SR> spinnerMessage(final String message) {
        spinnerProcessor.setMessage(message);
        return this;
    }


//    public MyAngelVolleyExecutor<SR> setSpinnerType(final SpinnerMessageType spinnerMessageType, final Object data) {
////        String message = spinnerProcessor.message;
//        switch (spinnerMessageType) {
//            case DEFAULT: {
//                spinnerProcessor = new DefaultSpinnerProcessor();
//                spinnerProcessor.init(context, "Attendere...");
//                break;
//            }
//            case CUSTOM: {
//
////                try {
//                String message = checkData(data, String.class);
//                spinnerProcessor.init(context, message);
////                    if (data == null) {
////                        throw new MissingPropertyException("progressDialog cannot be null");
////                    }
////
////                    ProgressDialog progressDialog = (ProgressDialog) data;
//
////                spinnerProcessor = new CustomSpinnerProcessor(progressDialog);
////                } catch (ClassCastException e) {
////                    throw new IncorrectPropertyException("The data for Spinner must be of type 'android.app.ProgressDialog'");
////                }
//            }
//            case NONE: {
//                spinnerProcessor = new NoSpinnerProcessor();
//                break;
//            }
//        }
//
////        spinnerProcessor.message = message;
//
//        return this;
//    }

    public MyAngelVolleyExecutor<SR> errorMessageDialog(final String message) {
        setErrorMessageType(MessageErrorType.CUSTOM_MESSAGE, message);
        return this;
    }

    public MyAngelVolleyExecutor<SR> setErrorMessageType(final MessageErrorType errorType) {
        setErrorMessageType(errorType, null);
        return this;
    }

    public MyAngelVolleyExecutor<SR> setErrorMessageType(MessageErrorType errorType, final Object data) {

        AlertDialog alertDialogCustom = options.getAlertDialogCustom(context);

        switch (errorType) {
            case DEFAULT: {
                errorMessageProcessor = new DefaultErrorMessageProcessor(alertDialogCustom);
                break;
            }
            case EXCEPTION: {
                errorMessageProcessor = new ErrorExceptionMessageProcessor(alertDialogCustom);
                break;
            }
            case CUSTOM_MESSAGE: {

                final String message = checkData(data, String.class);

                errorMessageProcessor = new DefaultErrorCustomMessageProcessor(alertDialogCustom) {
                    @Override
                    protected String getMessage(Context context, Exception e) {
                        return message;
                    }
                };


                break;
            }

            case NONE: {
                errorMessageProcessor = new NoErrorMessageProcessor(null);
                break;
            }
        }

        return this;

    }

    protected InitializedData initRequest() throws NoHandShakeException {

        String url = initUrl();

        if (method.ordinal() != Request.Method.GET) {
            if (requestBody == null) {
                throw new IllegalStateException("The Request body cannot be null");
            }

            requestBody.addBodyParameters(serviceBody);

            requestBody.setServiceName(service);
        }

        final String jsonBody = convertBodyToJson();


        addDefaultHeaders();

        logger.debug("Creating volley request\nUrl -> {}\nMethod -> {}\nService -> {}\nheaders -> {}", url, method, service, headers);

        return new InitializedData(url, jsonBody);
    }

    protected class InitializedData implements Serializable {
        private final String url;
        private final String jsonBody;

        public InitializedData(String url, String jsonBody) {
            this.url = url;
            this.jsonBody = jsonBody;
        }
    }


    private String initUrl() {

        logger.debug("Init request url");

        String authorization = headers.get("Authorization");
        String url;
        if (it.groupama.myangel.utilities.java.TextUtils.isEmptyNull(authorization)) {
            url = options.getUrlFree();
        } else {
            url = options.getUrlAuth();
        }

        if (TextUtils.isEmpty(url)) {
            throw new MissingPropertyException("The url been not defined");
        }

        if (method.ordinal() == Request.Method.GET) {
            url = UrlUtils.concatUrl(url, service);
        }

        if (!queryParameters.isEmpty()) {
            final String queryParametersValueCrypted = cryptManager.crypt(queryParameters);
            Map<String, String> queryParametersCrypted = new LinkedHashMap<>();
            queryParametersCrypted.put("body", queryParametersValueCrypted);

            UrlUtils.concatQueryParameters(url, queryParametersCrypted);

        }

        return url;

    }



    protected String convertBodyToJson() {
//        final String jsonBody = gson.toJson(requestBody);

        final String jsonBodyCrypt = cryptManager.crypt(requestBody);

        MapStringObject cryptedBody = new MapStringObject();
        cryptedBody.put("json", jsonBodyCrypt);

        return gson.toJson(cryptedBody);

    }

    private static final List<String> headersNameToSymmetricCrypt = new ArraySetList<String>(){
        {
            add(CRYPT_HEADER_KEY_EXCHANGESERVICE_SERVICE_NAME);
            add(CRYPT_HEADER_KEY_EXCHANGESERVICE_PUBLIC_KEY);
            add(CRYPT_HEADER_KEY_UUID);
            add(CRYPT_HEADER_KEY_USER_ID);
            add(CRYPT_HEADER_KEY_BUNDLE_ID);
        }
    };

    private void addDefaultHeaders() throws NoHandShakeException {

        logger.debug("Adding headers");


        for (Map.Entry<String, String> entry : headers.entrySet()) {
            if (headersNameToSymmetricCrypt.contains(entry.getKey())) {
                header(entry.getKey(), cryptManager.cryptSymmetric(entry.getValue()));
            }
        }


        header(CRYPT_HEADER_KEY_UUID, cryptManager.cryptSymmetric(options.getUuid()));
        header(CRYPT_HEADER_KEY_USER_ID, cryptManager.cryptSymmetric(requestBody.getDeviceId()));
        header(CRYPT_HEADER_KEY_BUNDLE_ID, cryptManager.cryptSymmetric(context.getPackageName()));

        header(HEADER_REQUEST_TOKEN, RequestTokenGenerator.generateToken());


        completeCryptProcess();
    }

    protected void completeCryptProcess() throws NoHandShakeException {
        try {

            boolean serviceNotEquals = !isHandshakeService();
            boolean handshake = !CryptKey.isHandshakeExecuted();

            if (serviceNotEquals && handshake) {
                throw new NoHandShakeException(context);
            }

//            if (!CryptKey.isHandshakeExecuted()) {
//                throw new NoHandShakeException();
//            }

            if (TextUtils.isEmpty(CryptKey.getPublicKeyServer())) {
                logger.error("Public key is null");
                return;
            }
            header(CRYPT_HEADER_KEY_DECRYPTER_KEY, cryptManager.complete(CryptKey.getPublicKeyServer()));
        } catch (CryptCheckException e) {
            logger.error("No data crypted for this session");
            logger.error(e.getMessage());
        }
    }

    public void executeRequest() throws NoHandShakeException {
        try {

            initSpinner();

            spinnerProcessor.show();

            checkPermission(context);

            RequestQueue requestQueue = Volley.newRequestQueue(context);

            logger.debug("Call before execute");
            beforeExecute();

            InitializedData initializedData = initRequest();

            VolleyRequest volleyRequest = new VolleyRequest(method.ordinal(), initializedData.url, initializedData.jsonBody, new Response.Listener<MyAngelResponse>() {
                @Override
                public void onResponse(MyAngelResponse response) {

                    final MyAngelResponse decryptedResponse = decryptResponse(response);
                    SR serviceResponse = null;

                    if (serviceResponseBodyClass != null) {
                        serviceResponse = convertObject(decryptedResponse.getResponseData(), serviceResponseBodyClass);
                    } else {
                        logger.error("No service class for convert present");
                    }

                    logger.debug("Call after execute");
                    afterExecute(decryptedResponse, serviceResponse);


                    if (successListener != null && srSuccessListener != null) {
                        throw new IllegalStateException("Only one success listener can be setted");
                    }

                    if (successListener != null) {
                        successListener.onSuccess(decryptedResponse, responseStatusCode);
                    }

                    if (srSuccessListener != null) {
                        srSuccessListener.onSuccess(decryptedResponse, serviceResponse, responseStatusCode);
                    }

                    spinnerProcessor.dismiss();
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {
                    error.printStackTrace();

                    MyAngelResponse response = getResponse(error);
                    logger.error(response);


                    if (errorListener != null && srErrorListener != null) {
                        throw new IllegalStateException("Only one error listener can be setted");
                    }


                    if (errorListener != null) {
                        errorListener.onError(response, error, error.networkResponse != null ? error.networkResponse.statusCode : -1);
                    }

                    if (srErrorListener != null) {
                        SR serviceResponse = null;
                        if (serviceResponseBodyClass != null && response != null) {
                            serviceResponse = convertObject(response.getResponseData(), serviceResponseBodyClass);
                        }

                        srErrorListener.onError(response, serviceResponse, error, error.networkResponse != null ? error.networkResponse.statusCode : -1);

                    }


//                    new AsyncTask<Void, Void, Void>() {
//                        @Override
//                        protected Void doInBackground(Void... params) {
//                            try {
//                                Thread.sleep(3000);
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }
//                            return null;
//                        }
//
//                        @Override
//                        protected void onPostExecute(Void aVoid) {
//                            super.onPostExecute(aVoid);
//
//                            errorMessageProcessor.showDialog(context, error);
//                            spinnerProcessor.dismiss();
//                        }
//
//                    }.execute();

                    errorMessageProcessor.showDialog(context, error);
                    spinnerProcessor.dismiss();

                }
            });


            final DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(options.getTimeOut(), options.getRetry(), DEFAULT_BACKOFF_MULT);
            volleyRequest.setRetryPolicy(retryPolicy);

//            logger.debug("Set retry policy with options\nTimeout ->{}\nMax retries -> {}\nBackoff -> {}", retryPolicy.getCurrentTimeout(), retryPolicy.getCurrentRetryCount(), retryPolicy.getBackoffMultiplier());

            logger.debug("Add request to queue");
            requestQueue.add(volleyRequest);


        } catch (InternetPermissionDeniedException e) {
            spinnerProcessor.dismiss();
            e.showAlert();
        } catch (Exception e) {
            if (!noStopSpinner.contains(e.getClass())) {
                spinnerProcessor.dismiss();
            }
            throw e;
        }

    }

    private MyAngelResponse decryptResponse(final MyAngelResponse response) {
        MyAngelResponse decryptedData = response;
        String deprypterKey = responseHeaders.get(CRYPT_HEADER_KEY_DECRYPTER_KEY);
        if (!TextUtils.isEmpty(deprypterKey)) {
            try {
                decryptedData = cryptManager.decrypt(response.<String>getResponseData(), deprypterKey, CryptKey.getPrivateKeyClient(), MyAngelResponse.class);
            } catch (Exception e) {
                logger.error(e);
            }
        }

        return decryptedData;
    }

    private <T> T convertObject(Object source, Class<T> convertClass) {
        final String json = gson.toJson(source);
        if (isJsonObject(json)) {
            return gson.fromJson(json, convertClass);
        }
        return null;
    }

    public MyAngelVolleyExecutor<SR> timeout(final int timeoutMs) {
        options.setSpecificOptions(OPTION_TIMEOUT, timeoutMs);
        return this;
    }

    public MyAngelVolleyExecutor<SR> retry(final int retry) {
        options.setSpecificOptions(OPTION_RETRY, retry);
        return this;
    }

    private void checkPermission(final Context context) throws InternetPermissionDeniedException {

        logger.debug("Check internet permission");

        final String internetPermission = Manifest.permission.INTERNET;
        int checkPermission = ContextCompat.checkSelfPermission(context, internetPermission);

        try {
            if (PackageManager.PERMISSION_GRANTED != checkPermission && !ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, internetPermission)) {
                logger.error("Internet permission not available");
                throw new InternetPermissionDeniedException(context);
            }

        } catch (ClassCastException e) {
            // Come se avesse i permessi
        }

        logger.debug("Internet permission available");


    }

    private boolean serviceCall;

    public MyAngelResponse getResponse() throws ExecutionException, InterruptedException, InternetPermissionDeniedException, NoHandShakeException {
        checkPermission(context);
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(context);

            beforeExecute();
            InitializedData initializedData = initRequest();

            RequestFuture<MyAngelResponse> future = RequestFuture.newFuture();
            VolleyRequest request = new VolleyRequest(method.ordinal(), initializedData.url, initializedData.jsonBody, future, future);
//        JsonObjectRequest request = new JsonObjectRequest(url, null, future, future);
            requestQueue.add(request);


            final MyAngelResponse response = future.get();

            final MyAngelResponse myAngelResponse = decryptResponse(response);

            if (!serviceCall) {
                afterExecute(myAngelResponse, null);
            }

            return myAngelResponse;
        } catch (ExecutionException error) {
            logger.error(error);
            throw error;
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }

    }

    private void throwVolleyError() throws VolleyError {
        // LO frego
    }


    public SR getServiceResponse() throws ExecutionException, InterruptedException, InternetPermissionDeniedException, NoHandShakeException {
        serviceCall = true;
        MyAngelResponse response = getResponse();

        // Creare eccezione con dentro la respnse

        SR serviceResponse = convertObject(response.getResponseData(), serviceResponseBodyClass);

        afterExecute(response, serviceResponse);

        serviceCall = false;
        return serviceResponse;
    }


    private MyAngelResponse getResponse(VolleyError error) {
        if (error.networkResponse != null) {

            responseHeaders = error.networkResponse.headers;
            responseStatusCode = error.networkResponse.statusCode;

            String json = new String(error.networkResponse.data);
            logger.error("Json response error -> {}", json);
            MyAngelResponse myAngelResponse = gson.fromJson(json, MyAngelResponse.class);

            if (isJsonObject(myAngelResponse.<String>getResponseData())) {
                return myAngelResponse;
            }

            return decryptResponse(myAngelResponse);
        }

        return null;
    }

    private boolean isJsonObject(final String data) {
        return data.startsWith("{") && data.endsWith("}");
    }

    private boolean isHandshakeService() {
        return "exchangeKeys".equals(service);
    }


    private class VolleyRequest extends JsonRequest<MyAngelResponse> {

        private VolleyRequest(int method, String url, String requestBody, Response.Listener<MyAngelResponse> listener, Response.ErrorListener errorListener) {
            super(method, url, requestBody, listener, errorListener);
        }

//        public BathPrenRequest(int method, String url, Object requestBody, Response.Listener<T> listener, Response.ErrorListener errorListener) {
//            super(method, url, gson.toJson(requestBody), listener, errorListener);
//        }

        @Override
        protected Response<MyAngelResponse> parseNetworkResponse(NetworkResponse response) {

//            if (responseBodyClass == null) {
//                throw new IllegalStateException("No Return class defined");
//            }

            responseStatusCode = response.statusCode;
            responseHeaders = response.headers;

            String json = new String(response.data);
            MyAngelResponse myAngelResponse = gson.fromJson(json, MyAngelResponse.class);
            return Response.success(myAngelResponse, HttpHeaderParser.parseCacheHeaders(response));
        }

        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            return headers;
        }
    }

//    private abstract class CustomErrorMessageProcessor extends AbstractErrorMessageProcessor {
//
//
//        @Override
//        protected String getMessage(Context context, Exception e) {
//            return "Si è verificato un errore improvviso";
//        }
//
//        @Override
//        void showDialog(Context context, Exception e) {
//            AlertDialog alertDialog = getAlertDialog(context, e);
//            alertDialog.setMessage(getMessage(context, e));
////            alertDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//            alertDialog.show();
//        }
//    }

    private class DefaultErrorMessageProcessor extends AbstractErrorMessageProcessor {

        public DefaultErrorMessageProcessor(AlertDialog alertDialog) {
            super(alertDialog);
        }

        @Override
        protected String getMessage(Context context, Exception e) {
            return "Si è verificato un errore improvviso";
        }
    }

    private class ErrorExceptionMessageProcessor extends AbstractErrorMessageProcessor {

        public ErrorExceptionMessageProcessor(AlertDialog alertDialog) {
            super(alertDialog);
        }

        @Override
        protected String getMessage(Context context, Exception e) {
            return e.getMessage();
        }
    }

    private abstract class DefaultErrorCustomMessageProcessor extends AbstractErrorMessageProcessor {

        public DefaultErrorCustomMessageProcessor(AlertDialog alertDialog) {
            super(alertDialog);
        }
    }

    private class NoErrorMessageProcessor extends AbstractErrorMessageProcessor {

        public NoErrorMessageProcessor(AlertDialog alertDialog) {
            super(alertDialog);
        }

        @Override
        void showDialog(Context context, Exception e) {
        }
    }


    private abstract class AbstractErrorMessageProcessor {
        //        String message = "Si è verificato un errore improvviso";
        private AlertDialog alertDialog;

        public AbstractErrorMessageProcessor(AlertDialog alertDialog) {
            this.alertDialog = alertDialog;
        }

        void showDialog(final Context context, Exception e) {
            AlertDialog alertDialog;
            if (this.alertDialog != null) {
                alertDialog = this.alertDialog;
            } else {
                alertDialog = new AlertDialog.Builder(context)
                        .setCancelable(false)
                        .setTitle("Attenzione")
                        .setNeutralButton("Ok", null)
                        .create();
            }

            final String message = getMessage(context, e);
            alertDialog.setMessage(message);
            alertDialog.show();
        }

        protected String getMessage(final Context context, Exception e) {
            return "";
        }
    }


    private static class DefaultSpinnerProcessor extends AbstractSpinnerProcessor {
    }

//    private class CustomSpinnerProcessor extends AbstractSpinnerProcessor {
//        private ProgressDialog progressDialog;
//
//        public CustomSpinnerProcessor(ProgressDialog progressDialog) {
//            this.progressDialog = progressDialog;
//        }
//    }


    private class NoSpinnerProcessor extends AbstractSpinnerProcessor {

        @Override
        void init(Context context) {
        }

        @Override
        public void show() {

        }

        @Override
        public void dismiss() {

        }
    }

    private static abstract class AbstractSpinnerProcessor {
        protected ProgressDialog progressDialog;

        void init(final Context context) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage("Attendere...");
        }

        void setProgressDialog(ProgressDialog progressDialog) {
            this.progressDialog = progressDialog;
        }

        public void setMessage(String message) {
            progressDialog.setMessage(message);
        }

        void show() {
            if (!progressDialog.isShowing()) {
                progressDialog.show();
            }
        }

        void dismiss() {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

        boolean isShowing() {
            return progressDialog != null && progressDialog.isShowing();
        }
    }

//    private interface SpinnerProcessor {
//        void show(final Context context);
//        void dismiss();
//    }


    public static void setOption(final String key, final Object value) {
        MyAngelVolleyExecutorOptions.setOptions(key, value);
    }

    public interface SuccessListener {
        void onSuccess(MyAngelResponse response, int statusCode);
    }

    public interface ErrorListener {
        void onError(MyAngelResponse response, Exception e, int statusCode);
    }


    public interface MyAngeSuccessListener<SR> {
        void onSuccess(MyAngelResponse response, SR serviceResponse, int statusCode);
    }

    public interface MyAngeErrorListener<SR> {
        void onError(MyAngelResponse response, SR serviceResponse, Exception e, int statusCode);
    }


    public enum MessageErrorType {
        DEFAULT, EXCEPTION, CUSTOM_MESSAGE, NONE
    }

//    public enum SpinnerMessageType {
//        DEFAULT, CUSTOM, NONE
//    }

    private <T> T checkData(Object data, Class<T> castClass) {
        try {

            if (data == null) {
                throw new IllegalStateException(castClass.getSimpleName() + " cannot be null");
            }

            return castClass.cast(data);

        } catch (ClassCastException e) {
            //noinspection ConstantConditions
            throw new IllegalStateException("Data type is not correct. It must be " + castClass.getName());
        }


    }
    public enum Method {
        GET, POST, PUT, DELETE, HEAD, OPTIONS, TRACE, PATCH;

        public static Method valueOf(int value) {
            for (Method method : values()) {
                if (method.ordinal() == value) {
                    return method;
                }
            }

            return null;

        }
    }

    protected void beforeExecute() {

    }

    protected void afterExecute(final MyAngelResponse response, final SR serviceResponse) {

    }
    public String getService() {
        return service;
    }


    protected void showSpinner() {
        spinnerProcessor.show();
    }

    protected void dismissSpinner() {
        spinnerProcessor.dismiss();
    }
}
