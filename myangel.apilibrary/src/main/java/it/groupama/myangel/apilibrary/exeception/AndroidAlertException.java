package it.groupama.myangel.apilibrary.exeception;

import android.content.Context;

import it.groupama.myangel.apilibrary.alert.AlertDialogErrorUtils;


/**
 * Created by Francesco in 19/07/17.
 */

public class AndroidAlertException extends Exception {

    private final Context context;

    public AndroidAlertException(final Context context) {
        this.context = context;
    }

    public AndroidAlertException(final Context context, String message) {
        super(message);
        this.context = context;
    }

    public AndroidAlertException(final Context context, String message, Throwable cause) {
        super(message, cause);
        this.context = context;
    }

    public AndroidAlertException(final Context context, Throwable cause) {
        super(cause);
        this.context = context;
    }

    public void showAlert() {
        AlertDialogErrorUtils.showSimpleAlertDialog(context, getMessage());
    }
}
