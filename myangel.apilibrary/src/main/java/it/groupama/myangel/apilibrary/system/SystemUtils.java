package it.groupama.myangel.apilibrary.system;

import android.os.Build;

import it.groupama.myangel.utilities.logger.Logger;

/**
 * Created by Francesco in 24/07/17.
 */

public abstract class SystemUtils {
    private static final Logger LOGGER = Logger.getIntance(SystemUtils.class);

    protected SystemUtils() {
    }


    public static boolean isEmulator() {
        LOGGER.debug("Check for Emulator PRODUCT {}, MODEL {}", Build.PRODUCT, Build.MODEL);
        boolean sdk_google = Build.PRODUCT.contains("sdk_google");
        LOGGER.debug("Result check {}", sdk_google);
        return sdk_google;
    }
}
