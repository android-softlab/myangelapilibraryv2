package it.groupama.myangel.apilibrary.executor.crypt.exception;

import android.content.Context;

import it.groupama.myangel.apilibrary.exeception.AndroidAlertException;

/**
 * Created by Francesco in 14/06/17.
 */

public class NoHandShakeException extends AndroidAlertException {

    public NoHandShakeException(Context context) {
        this(context, "Non è stata effettuato l'handshake");
    }

    public NoHandShakeException(Context context, String message) {
        super(context, message);
    }

    public NoHandShakeException(Context context, String message, Throwable cause) {
        super(context, message, cause);
    }

    public NoHandShakeException(Context context, Throwable cause) {
        super(context, cause);
    }
}
