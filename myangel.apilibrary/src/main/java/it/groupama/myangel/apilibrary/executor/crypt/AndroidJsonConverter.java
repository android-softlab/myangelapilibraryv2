package it.groupama.myangel.apilibrary.executor.crypt;

import com.google.gson.Gson;

import it.groupama.security.crypt.exception.JsonConverterException;
import it.groupama.security.crypt.json.SecurityCryptJsonConverter;

/**
 * Created by Francesco in 18/05/17.
 */

public class AndroidJsonConverter implements SecurityCryptJsonConverter {


    private final Gson gson;

    public AndroidJsonConverter(Gson gson) {
        this.gson = gson;
    }

    @Override
    public String convertToJson(Object o) throws JsonConverterException {
        return gson.toJson(o);
    }

    @Override
    public <T> T convertToData(String s, Class<T> aClass) throws JsonConverterException {
        return gson.fromJson(s, aClass);
    }

    @Override
    public <T> T convertToData(Object o, Class<T> aClass) throws JsonConverterException {
        String json = gson.toJson(o);
        return gson.fromJson(json, aClass);
    }
}
