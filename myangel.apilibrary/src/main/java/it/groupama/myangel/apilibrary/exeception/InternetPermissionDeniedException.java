package it.groupama.myangel.apilibrary.exeception;

import android.content.Context;

/**
 * Created by Francesco in 19/07/17.
 */

public class InternetPermissionDeniedException extends AndroidAlertException {

    public InternetPermissionDeniedException(final Context context) {
        super(context, "Non si dispone dei permessi di accesso ad internet");
    }

}
