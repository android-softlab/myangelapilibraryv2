package it.groupama.myangel.apilibrary.executor;

import android.content.Context;

import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;

import it.groupama.myangel.apilibrary.exeception.InternetPermissionDeniedException;
import it.groupama.myangel.apilibrary.exeception.MyAngelApiException;
import it.groupama.myangel.apilibrary.executor.crypt.CryptKey;
import it.groupama.myangel.apilibrary.executor.crypt.exception.NoHandShakeException;
import it.groupama.myangel.apilibrary.response.HandshakeResponse;
import it.groupama.myangel.apilibrary.response.MyAngelResponse;
import it.groupama.myangel.utilities.java.MapStringObject;
import it.groupama.security.crypt.impl.CryptManager;
import it.groupama.security.crypt.model.PairKey;


/**
 * Created by Francesco in 24/07/17.
 */

public class HandShakeExecutor extends MyAngelVolleyExecutor<HandshakeResponse> {

    private static final String SERVICE_NAME = "exchangeKeys";

    public HandShakeExecutor(Context context) {
        super(context);
    }

    @Override
    protected void init() {
        service(SERVICE_NAME);
        method(Method.POST);
        header(MyAngelVolleyExecutor.CRYPT_HEADER_KEY_EXCHANGESERVICE_SERVICE_NAME, SERVICE_NAME);
        serviceResponseClass(HandshakeResponse.class);
    }


    @Override
    protected MyAngelVolleyExecutor<HandshakeResponse>.InitializedData initRequest() throws NoHandShakeException {

        try {
            final PairKey keys = cryptManager.generatePairKeys("RSA");

            CryptKey.setPrivateKeyClient(CryptManager.encodeBase64(keys.getPrivateKey()));

            header(MyAngelVolleyExecutor.CRYPT_HEADER_KEY_EXCHANGESERVICE_PUBLIC_KEY, CryptManager.encodeBase64(keys.getPublicKey()));

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new MyAngelApiException(context, e);
        }

        return super.initRequest();
    }

    @Override
    public final void executeRequest() throws NoHandShakeException {
        throw new UnsupportedOperationException("Use executeHandShake method");
    }

    public void executeHandShake() {
        try {
            super.executeRequest();
        } catch (NoHandShakeException e) {
            e.printStackTrace();
        }
    }

    @Override
    public HandshakeResponse getServiceResponse() throws ExecutionException, InterruptedException, InternetPermissionDeniedException, NoHandShakeException {
        throw new UnsupportedOperationException("Use executeHandShake method");
    }

//    @Override
//    public MyAngelResponse getResponse() throws ExecutionException, InterruptedException, InternetPermissionDeniedException, NoHandShakeException {
//        throw new UnsupportedOperationException("Use executeHandShake method");
//    }


    public HandshakeResponse getHandShakeServiceResponse() throws ExecutionException, InterruptedException, InternetPermissionDeniedException {
        try {
            return super.getServiceResponse();
        } catch (NoHandShakeException e) {
            e.printStackTrace();
        }

        return null;
    }

//    public MyAngelResponse getHandShakeResponse() throws ExecutionException, InterruptedException, InternetPermissionDeniedException {
//        try {
//            return super.getResponse();
//        } catch (NoHandShakeException e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }


    @Override
    protected void beforeExecute() {
        super.beforeExecute();
        requestBody.addParameter("os", requestBody.getOs());
    }

    @Override
    protected void afterExecute(MyAngelResponse response, HandshakeResponse serviceResponse) {
        super.afterExecute(response, serviceResponse);
        CryptKey.setPublicKeyServer(serviceResponse.getPublicKeyServer());
    }

//    @Override
//    protected void completeCryptProcess() {
////        super.completeCryptProcess();
//    }

        protected String convertBodyToJson() {
//        final String jsonBody = gson.toJson(requestBody);

        final String jsonBodyCrypt = cryptManager.cryptSymmetric(requestBody);

        MapStringObject cryptedBody = MapStringObject.getInstance("json", jsonBodyCrypt);

        return gson.toJson(cryptedBody);

    }

    //    new MyAngelVolleyExecutor<HandshakeResponse>(context)
//            .service("exchangeKeys")
//                .method(MyAngelVolleyExecutor.Method.POST)
//                .body(request)
//                .header(MyAngelVolleyExecutor.CRYPT_HEADER_KEY_EXCHANGESERVICE_SERVICE_NAME, "exchangeKeys")
//                .success(successListener)
//                .error(errorListener)
//                .executeRequest();

}
