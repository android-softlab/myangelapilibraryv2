package it.groupama.myangel.apilibrary.exeception;

import android.content.Context;

/**
 * Created by Francesco in 24/07/17.
 */

public class MyAngelApiException extends AndroidAlertRuntimeException {
    public MyAngelApiException(Context context) {
        super(context);
    }

    public MyAngelApiException(Context context, String message) {
        super(context, message);
    }

    public MyAngelApiException(Context context, String message, Throwable cause) {
        super(context, message, cause);
    }

    public MyAngelApiException(Context context, Throwable cause) {
        super(context, cause);
    }
}
