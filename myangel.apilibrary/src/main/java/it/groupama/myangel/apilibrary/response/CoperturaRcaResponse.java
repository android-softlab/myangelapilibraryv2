package it.groupama.myangel.apilibrary.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Francesco in 19/07/17.
 */

public class CoperturaRcaResponse implements Serializable {

    @SerializedName("EsitoElaborazione")
    private int esitoElaborazione;

    public int getEsitoElaborazione() {
        return esitoElaborazione;
    }

    public void setEsitoElaborazione(int esitoElaborazione) {
        this.esitoElaborazione = esitoElaborazione;
    }
}
