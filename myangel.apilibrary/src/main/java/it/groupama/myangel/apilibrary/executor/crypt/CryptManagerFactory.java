package it.groupama.myangel.apilibrary.executor.crypt;

import com.google.gson.GsonBuilder;

import it.groupama.security.crypt.MemorySecurityCryptManager;
import it.groupama.security.crypt.impl.CryptManager;
import it.groupama.security.crypt.impl.MyACryptManagerFactory;
import it.groupama.security.crypt.model.CryptManagerProperties;

/**
 * Created by Francesco in 18/05/17.
 */

public class CryptManagerFactory {
    protected CryptManagerFactory() {
    }


    private static final MemorySecurityCryptManager cryptManager = MyACryptManagerFactory.getMemory();

    private static final CryptManagerProperties initializerProperties = new CryptManagerProperties();
    static {
        initializerProperties.setSymmetricKey("AIzaSyDSTNcCwQvm6wjLOsifW73CNn87hY4C5Y4");
        initializerProperties.setSymmetricVector(new byte[] {4, 3, 7, 5, 55, 3, 33, 2, 5, 1, 6, 2, 8, 8, 3, 0});
        initializerProperties.setAsymmetricLengthKeys(2048);

        initializerProperties.setLogger(new AndroidCryptLogger(CryptManager.class));
        initializerProperties.setJsonConverter(new AndroidJsonConverter(new GsonBuilder().create()));
        initializerProperties.setBase64Encoder(new AndroidBase64Encoder());

        cryptManager.init(initializerProperties);

    }


    public static MemorySecurityCryptManager getInstance() {
        return cryptManager;

    }




}
