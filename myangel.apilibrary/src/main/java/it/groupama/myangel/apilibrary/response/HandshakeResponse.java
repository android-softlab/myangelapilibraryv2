package it.groupama.myangel.apilibrary.response;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Francesco in 14/06/17.
 */

public class HandshakeResponse implements Serializable {
    private String publicKeyServer;
    private List<String> pin;
    private CheckForUpdateBean checkForUpdate;


    public String getPublicKeyServer() {
        return publicKeyServer;
    }

    public void setPublicKeyServer(String publicKeyServer) {
        this.publicKeyServer = publicKeyServer;
    }


    public List<String> getPin() {
        return pin;
    }

    public void setPin(List<String> pin) {
        this.pin = pin;
    }

    public CheckForUpdateBean getCheckForUpdate() {
        return checkForUpdate;
    }

    public void setCheckForUpdate(CheckForUpdateBean checkForUpdate) {
        this.checkForUpdate = checkForUpdate;
    }
}
