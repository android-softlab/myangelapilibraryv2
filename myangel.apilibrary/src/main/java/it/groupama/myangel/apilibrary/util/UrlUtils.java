package it.groupama.myangel.apilibrary.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import it.groupama.myangel.utilities.java.TextUtils;

/**
 * Created by Francesco in 20/07/17.
 */

public abstract class UrlUtils {
    protected UrlUtils() {
    }


    public static String concatUrl(final String url, final String...paths) {
        final StringBuilder builder = new StringBuilder(url);

        if (!url.endsWith("/")) {
            builder.append("/");
        }

        for (String path : paths) {
            builder.append(path).append("/");
        }

        TextUtils.replaseLastString(builder, "/");

        return builder.toString();
    }

    public static String concatQueryParameters(final String url, final Map<String, String> queryParameters) {
        final StringBuilder builder = new StringBuilder(url);

        if (!queryParameters.isEmpty()) {

            builder.append("?");

            for (Map.Entry<String, String> entry : queryParameters.entrySet()) {
                try {
                    builder.append(entry.getKey()).append("=").append(URLEncoder.encode(entry.getValue(), "UTF-8")).append("&");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            TextUtils.replaseLastString(builder, "&");


        }


        return builder.toString();
    }
}
