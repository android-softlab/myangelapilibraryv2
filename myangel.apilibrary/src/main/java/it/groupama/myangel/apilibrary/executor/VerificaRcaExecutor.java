package it.groupama.myangel.apilibrary.executor;

import android.content.Context;

import it.groupama.myangel.apilibrary.response.CoperturaRcaResponse;


/**
 * Created by Francesco in 24/07/17.
 */

public class VerificaRcaExecutor extends MyAngelVolleyExecutor<CoperturaRcaResponse> {

    public static final String SERVICE_NAME = "exchangeKeys";

    public VerificaRcaExecutor(Context context) {
        super(context);
    }

    @Override
    protected void init() {
        super.init();
        service("verificaRCA");
        method(Method.POST);
        serviceResponseClass(CoperturaRcaResponse.class);
    }

}
