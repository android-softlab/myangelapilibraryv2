package it.groupama.myangel.apilibrary.exeception;

import android.content.Context;

import it.groupama.myangel.api.alert.AlertDialogErrorUtils;

/**
 * Created by Francesco in 19/07/17.
 */

public class AndroidAlertRuntimeException extends RuntimeException {

    private final Context context;

    public AndroidAlertRuntimeException(final Context context) {
        this.context = context;
    }

    public AndroidAlertRuntimeException(final Context context, String message) {
        super(message);
        this.context = context;
    }

    public AndroidAlertRuntimeException(final Context context, String message, Throwable cause) {
        super(message, cause);
        this.context = context;
    }

    public AndroidAlertRuntimeException(final Context context, Throwable cause) {
        super(cause);
        this.context = context;
    }

    public void showAlert() {
        AlertDialogErrorUtils.showSimpleAlertDialog(context, getMessage());
    }

}
