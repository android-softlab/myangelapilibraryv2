package it.groupama.myangel.apilibrary.executor.crypt;

import android.util.Base64;

import it.groupama.security.crypt.encoder.Base64Encoder;

/**
 * Created by Francesco in 18/05/17.
 */

public class AndroidBase64Encoder implements Base64Encoder {


    private final int defaultInit = Base64.NO_WRAP;

    @Override
    public String encodeBase64(String s) {
        return Base64.encodeToString(s.getBytes(), defaultInit);
    }

    @Override
    public String encodeBase64(byte[] bytes) {
        return Base64.encodeToString(bytes, defaultInit);
    }

    @Override
    public byte[] decodeBase64(String s) {
        return Base64.decode(s, defaultInit);
    }

    @Override
    public String decodeBase64String(String s) {
        return new String(Base64.decode(s, defaultInit));
    }
}
