package it.groupama.myangel.apilibrary.executor;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;

import com.android.volley.DefaultRetryPolicy;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import it.groupama.myangel.utilities.java.MapStringObject;

/**
 * Created by Francesco in 20/07/17.
 */

public class MyAngelVolleyExecutorOptions {

    public static final String OPTION_URL_FREE = "urlFreeOption";
    public static final String OPTION_URL_AUTH = "urlAuthOption";

    public static final String OPTION_UUID = "uuidOption";

    public static final String OPTION_TIMEOUT = "timeoutOption";
    public static final String OPTION_RETRY = "retryOption";

    public static final String OPTION_SPINNER_SHOW = "spinnerShowOption";
    public static final String OPTION_SPINNER_MESSAGE = "spinnerMessageOption";
    public static final String OPTION_SPINNER_CUSTOM = "spinnerCustomOption";

    public static final String OPTION_ERROR_MESSAGE_DIALOG_TYPE = "errorDialogMessageTypeOption";
    public static final String OPTION_ERROR_MESSAGE_DIALOG_DATA = "errorDialogMessageDataOption";
    public static final String OPTION_ERROR_DIALOG_CUSTOM = "errorDialogCustomOption";



    private static final MapStringObject options = new MapStringObject();

    private final MapStringObject optionsSpecific = new MapStringObject();


    public String getUrlAuth() {
        return getValue(OPTION_URL_AUTH);
    }

    public String getUrlFree() {
        return getValue(OPTION_URL_FREE);
    }

    public int getRetry() {
        int retry = getInt(OPTION_RETRY);

        if (retry == 0) {
            retry = DefaultRetryPolicy.DEFAULT_MAX_RETRIES;
        }

        return retry;

    }

    public int getTimeOut() {
        int timeout = getInt(OPTION_TIMEOUT);

        if (timeout == 0) {
            timeout = DefaultRetryPolicy.DEFAULT_TIMEOUT_MS;
        }

        return timeout;
    }


    public ErrorMessageDialogData getMessageErrorType() {
        MyAngelVolleyExecutor.MessageErrorType messageErrorType = getValue(OPTION_ERROR_MESSAGE_DIALOG_TYPE);

        if (messageErrorType == null) {
            messageErrorType = MyAngelVolleyExecutor.MessageErrorType.DEFAULT;
        }

        return new ErrorMessageDialogData(messageErrorType, (String) getValue(OPTION_ERROR_MESSAGE_DIALOG_DATA));

    }

    public AlertDialog getAlertDialogCustom(final Context context) {
        AlertDialog alertDialog = null;
        Class<?> value = getValue(OPTION_ERROR_DIALOG_CUSTOM);
        if (value != null) {
            try {
                Constructor<?> constructor = value.getConstructor(Context.class);
                alertDialog = (AlertDialog) constructor.newInstance(context);

            } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return alertDialog;

    }

    public String getUuid() {
        return getValue(OPTION_UUID);
    }


    public boolean isShowSpinner() {
        return getBoolean(OPTION_SPINNER_SHOW, true);
    }

    public String getSpinnerMessage() {
        return getValue(OPTION_SPINNER_MESSAGE);
    }

    public ProgressDialog getCustomProgressDialog(final Context context) {
        ProgressDialog progressDialog = null;
        Class<?> value = getValue(OPTION_SPINNER_CUSTOM);
        if (value != null) {
            try {
                Constructor<?> constructor = value.getConstructor(Context.class);
                progressDialog = (ProgressDialog) constructor.newInstance(context);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
                e.printStackTrace();
            }

        }

        return progressDialog;
    }


    class ErrorMessageDialogData extends OptionData {
        private MyAngelVolleyExecutor.MessageErrorType errorType;

        public ErrorMessageDialogData(MyAngelVolleyExecutor.MessageErrorType errorType, Object data) {
            super(data);
            this.errorType = errorType;
        }

        public MyAngelVolleyExecutor.MessageErrorType getErrorType() {
            return errorType;
        }
    }


    class OptionData implements Serializable {
        private Object data;

        public OptionData(Object data) {
            this.data = data;
        }

        public Object getData() {
            return data;
        }
    }


    private Object getData(final String key) {
        Object data = optionsSpecific.get(key);
        if (data == null) {
            data = options.get(key);
        }

        return data;
    }


    public <T> T getValue(final String key) {
        T data = optionsSpecific.getValue(key);
        if (data == null) {
            data = options.getValue(key);
        }

        return data;
    }

    public byte getByte(String key) {
        Object value = this.getData(key);
        return value != null ? (Byte) value : 0;
    }

    public short getShort(String key) {
        Object value = this.getData(key);
        return value != null ? (Short) value : 0;
    }

    public int getInt(String key) {
        Object value = this.getData(key);
        return value != null ? (Integer) value : 0;
    }

    public long getLong(String key) {
        Object value = this.getData(key);
        return value != null ? (Long) value : 0L;
    }

    public float getFloat(String key) {
        Object value = this.getData(key);
        return value != null ? (Float) value : 0.0F;
    }

    public double getDouble(String key) {
        Object value = this.getData(key);
        return value != null ? (Double) value : 0.0D;
    }

    public char getChar(String key) {
        Object value = this.getData(key);
        return value != null ? (Character) value : '\u0000';
    }

    public boolean getBoolean(String key, final boolean defaultValue) {
        Object value = this.getData(key);
        if (value == null) {
            return defaultValue;
        }
        return (Boolean) value;
    }

    public static void setOptions(final String key, final Object value) {
        options.put(key, value);
    }

    public void setSpecificOptions(final String key, final Object value) {
        optionsSpecific.put(key, value);
    }

}
