package it.groupama.myangel.apilibrary.request;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Francesco in 19/07/17.
 */

public class MyAngelRequest implements Serializable {

    public static final String REQUEST_PROPERTIES = "requestProperties";
    public static final String KEY_BODY = "body";
    public static final String KEY_IDAPP = "idApp";

    private String userId;

    private String deviceId;

    private String os;

    private String osVersion;

    private String serviceName;


    private Map<String, Object> requestProperties;

    private Map<String, String> requestParams;

    public MyAngelRequest() {
        requestProperties = new LinkedHashMap<>();
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Map<String, Object> getRequestProperties() {
        return requestProperties;
    }

    public void setRequestProperties(Map<String, Object> requestProperties) {
        this.requestProperties = requestProperties;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }


    @SuppressWarnings("unchecked")
    public <T> T getBody() {
        return (T) requestProperties.get(KEY_BODY);
    }

    @SuppressWarnings("unchecked")
    public <P> P getRequestProperty(String keyProperty) {
        return (P) requestProperties.get(keyProperty);
    }

    public void addBodyParameters(Object bodyValue) {
        requestProperties.put(KEY_BODY, bodyValue);
    }

    public void addParameter(String key, Object value) {
        requestProperties.put(key, value);
    }

    @Override
    public String toString() {
        return "MyAServiceRequest{" + "serviceName='" + serviceName + '\'' + ", requestProperties=" + requestProperties + '}';
    }

    public Map<String, String> getRequestParams() {
        return requestParams;
    }

    public String getRequestParam(String key) {
        return requestParams.get(key);
    }

    public void setRequestParameters(Map<String, String> requestParams) {
        this.requestParams = requestParams;

    }

}
