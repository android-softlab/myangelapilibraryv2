package it.groupama.myangel.apilibrary.alert;

import android.content.Context;
import android.support.v7.app.AlertDialog;

import it.groupama.myangel.apilibrary.R;


/**
 * Created by Francesco in 19/07/17.
 */

public abstract class AlertDialogErrorUtils {
    protected AlertDialogErrorUtils() {
    }


    public static void showSimpleAlertDialog(final Context context, final String message) {
        new AlertDialog.Builder(context)
                .setIcon(R.drawable.ic_warning_black_24dp)
                .setTitle(android.R.string.dialog_alert_title)
                .setMessage(message)
                .setCancelable(false)
                .setNeutralButton("Ok", null)
                .create().show();
    }
}
