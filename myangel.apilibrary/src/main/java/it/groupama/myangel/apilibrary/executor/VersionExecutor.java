package it.groupama.myangel.apilibrary.executor;

import android.content.Context;

/**
 * Created by Francesco in 25/07/17.
 */

public class VersionExecutor extends MyAngelVolleyExecutor {

    public VersionExecutor(final Context context, final VersionType versionType) {
        super(context);
        service(versionType.name);
        method(Method.GET);
    }



    public enum VersionType {
        AGENZIE("agenzieVersion"), CARROZZERIE("carrozzerieVersion");

        private String name;

        VersionType(final String name) {
            this.name = name;
        }
    }
}
