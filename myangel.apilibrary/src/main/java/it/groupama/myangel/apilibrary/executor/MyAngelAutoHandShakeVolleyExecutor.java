package it.groupama.myangel.apilibrary.executor;

import android.content.Context;
import android.os.AsyncTask;

import it.groupama.myangel.apilibrary.executor.crypt.CryptKey;
import it.groupama.myangel.apilibrary.executor.crypt.exception.NoHandShakeException;
import it.groupama.myangel.apilibrary.response.HandshakeResponse;


/**
 * Created by Francesco in 26/07/17.
 */

public class MyAngelAutoHandShakeVolleyExecutor<SR> extends MyAngelVolleyExecutor<SR> {

    MyAngelAutoHandShakeVolleyExecutor(Context context) {
        super(context);
    }

    @Override
    public void executeRequest() throws NoHandShakeException {
        try {
            super.executeRequest();
        } catch (final NoHandShakeException e) {

            new AsyncTask<Void, Integer, Void>() {

                @Override
                protected Void doInBackground(Void... params) {

                    HandShakeExecutor handShakeExecutor = new HandShakeExecutor(context);
                    handShakeExecutor.body(getRequestBody());

                    for (int i = 0; i < 3; i++) {

                        try {
                            HandshakeResponse handShakeServiceResponse = handShakeExecutor.getHandShakeServiceResponse();
                            CryptKey.setPublicKeyServer(handShakeServiceResponse.getPublicKeyServer());
                            if (CryptKey.isHandshakeExecuted()) {
                                SR serviceResponse = getServiceResponse();
                                break;
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            break;

//                        } catch (ExecutionException | InterruptedException | InternetPermissionDeniedException e1) {
//                            e1.printStackTrace();
//                            break;
                        }

                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);

                }
            }.execute();
        }

    }

    @Override
    protected void beforeExecute() {
        super.beforeExecute();
        noStopSpinner.add(NoHandShakeException.class);
    }

}
