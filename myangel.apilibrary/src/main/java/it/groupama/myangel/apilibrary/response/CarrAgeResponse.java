package it.groupama.myangel.apilibrary.response;

import java.io.Serializable;

/**
 * Created by Francesco in 25/07/17.
 */

public class CarrAgeResponse implements Serializable {
    private String entity;
    private int versione;

    public CarrAgeResponse(String entity, int versione) {
        this.entity = entity;
        this.versione = versione;
    }

    public String getEntity() {
        return entity;
    }

    public int getVersione() {
        return versione;
    }

    @Override
    public String toString() {
        return "CarrAgeResponseObject{" +
                "entity='" + entity + '\'' +
                ", versione=" + versione +
                '}';
    }

}
