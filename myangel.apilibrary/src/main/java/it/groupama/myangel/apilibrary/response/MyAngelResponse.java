package it.groupama.myangel.apilibrary.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Francesco in 19/07/17.
 */

public class MyAngelResponse implements Serializable {

    private static final long serialVersionUID = -5188356788687233224L;

    public static final class Message implements Serializable {
        private static final long serialVersionUID = 4654330652121162904L;
        private String code;
        private String description;

        public Message() {
        }

        public Message(String code, String description) {
            this.code = code;
            this.description = description;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            Message message = (Message) o;

            return code.equals(message.code);
        }

        @Override
        public int hashCode() {
            return code.hashCode();
        }

        @Override
        public String toString() {
            return "Message{" +
                    "code='" + code + '\'' +
                    ", description='" + description + '\'' +
                    '}';
        }
    }

    private Map<String,Object> responseProperties = new LinkedHashMap<>();

    private List<Message> messages = new ArrayList<>();

    public Map<String, Object> getResponseProperties() {
        return responseProperties;
    }

    public void setResponseProperties(Map<String, Object> responseProperties) {
        this.responseProperties = responseProperties;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public void addMessage(String code, String description){
        messages.add(new Message(code, description));
    }

    public void addMessage(int code, String description) {
        messages.add(new Message(String.valueOf(code), description));
    }

    public void addResponseProperty(String propertyName, Object propertyValue) {
        responseProperties.put(propertyName, propertyValue);
    }

    private static final String KEY_RESPONSE_DATA = "responseData";

    public void addResponseData(Object propertyValue) {
        addResponseProperty(KEY_RESPONSE_DATA, propertyValue);
    }

    @SuppressWarnings("unchecked")
    public <T> T getPropertyResponseData(String key) {
        return (T) ((Map<String, Object>) responseProperties.get(KEY_RESPONSE_DATA)).get(key);

    }

    @SuppressWarnings("unchecked")
    public <T> T getResponseData() {
        return (T) responseProperties.get(KEY_RESPONSE_DATA);
    }


    @Override
    public String toString() {
        return "MyAServiceResponse{" +
                "responseProperties=" + responseProperties +
                ", messages=" + messages +
                '}';
    }

    public static MyAngelResponse getResponse(int code, String message) {
        MyAngelResponse myAngelResponse = new MyAngelResponse();
        myAngelResponse.addMessage(code, message);
        return myAngelResponse;
    }

    public static MyAngelResponse getResponse(String code, String message) {
        MyAngelResponse myAngelResponse = new MyAngelResponse();
        myAngelResponse.addMessage(code, message);
        return myAngelResponse;
    }

}
