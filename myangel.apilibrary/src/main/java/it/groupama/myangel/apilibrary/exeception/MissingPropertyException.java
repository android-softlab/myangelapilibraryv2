package it.groupama.myangel.apilibrary.exeception;

/**
 * Created by Francesco in 20/07/17.
 */

public class MissingPropertyException extends RuntimeException {

    public MissingPropertyException() {
    }

    public MissingPropertyException(String message) {
        super(message);
    }

    public MissingPropertyException(String message, Throwable cause) {
        super(message, cause);
    }

    public MissingPropertyException(Throwable cause) {
        super(cause);
    }
}
