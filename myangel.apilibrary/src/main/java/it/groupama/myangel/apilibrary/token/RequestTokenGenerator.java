package it.groupama.myangel.apilibrary.token;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import it.groupama.myangel.apilibrary.system.SystemUtils;
import it.groupama.myangel.utilities.logger.Logger;

/**
 * Created by Francesco in 12/05/17.
 */

@SuppressWarnings("StringBufferReplaceableByString")
public class RequestTokenGenerator {


    private static final Logger logger = Logger.getIntance(RequestTokenGenerator.class);

    private static final String devicePrefix = "AD";
    private static final String emulatorPrefix = "AS";

    public static String generateToken() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.ITALY);
        Date date = Calendar.getInstance().getTime();
        String currentDate = simpleDateFormat.format(date);
        StringBuilder stringBuilder = new StringBuilder(SystemUtils.isEmulator() ? emulatorPrefix : devicePrefix).append('-').append(currentDate).append(generateUUID());
        logger.debug("generated request token -> {}", stringBuilder);
        return stringBuilder.toString();
    }

    /**
     * Metodo che genera 2 UUID e li concatena, facendo poi un substring in modo da poter
     * raggiungere i 64 caratteri nel token
     *
     * @return
     */
    private static String generateUUID() {
        UUID uuid = UUID.randomUUID();
        UUID uuid1 = UUID.randomUUID();
        String uuidConcat = uuid.toString().concat(uuid1.toString());
        return uuidConcat.substring(0, 44);
    }

}
