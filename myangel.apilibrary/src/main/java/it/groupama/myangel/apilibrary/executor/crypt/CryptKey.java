package it.groupama.myangel.apilibrary.executor.crypt;

import it.groupama.myangel.utilities.java.TextUtils;

/**
 * Created by Francesco in 18/05/17.
 */

@SuppressWarnings("StringBufferReplaceableByString")
public class CryptKey {

    private static String privateKeyClient;
    private static String publicKeyServer;


    public static String getPrivateKeyClient() {
        return privateKeyClient;
    }

    public static void setPrivateKeyClient(String privateKeyClient) {
        CryptKey.privateKeyClient = privateKeyClient;
    }

    public static String getPublicKeyServer() {
        return publicKeyServer;
    }

    public static void setPublicKeyServer(String publicKeyServer) {
        CryptKey.publicKeyServer = publicKeyServer;
    }

    public static boolean isHandshakeExecuted() {
        return !TextUtils.isEmptyNull(privateKeyClient) && !TextUtils.isEmptyNull(publicKeyServer);
    }

    public static String prettyPrintKey() {
        final StringBuilder builder = new StringBuilder(CryptKey.class.getName());

        builder.append(" {\n");

        builder.append("\t").append("privateKeyClient -> ").append(privateKeyClient).append("\n");

        builder.append("\t").append("publicKeyServer -> ").append(publicKeyServer).append("\n}");

        return builder.toString();
    }
}
