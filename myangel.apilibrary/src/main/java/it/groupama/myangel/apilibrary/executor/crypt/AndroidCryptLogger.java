package it.groupama.myangel.apilibrary.executor.crypt;

import it.groupama.myangel.utilities.logger.Logger;
import it.groupama.security.crypt.logger.SecurityCryptLogger;

/**
 * Created by Francesco in 18/05/17.
 */

public class AndroidCryptLogger implements SecurityCryptLogger {

    private final Logger logger;

    public AndroidCryptLogger(final Class<?> clazz) {
        logger = Logger.getIntance(clazz);
    }

    @Override
    public void info(String s, Object... objects) {
        logger.debug(s, objects);
    }

    @Override
    public void warn(String s, Object... objects) {
        logger.warn(s);
    }

    @Override
    public void debug(String s, Object... objects) {
        logger.debug(s, objects);
    }

    @Override
    public void error(String s, Object... objects) {
        logger.error(s, objects);
    }

    @Override
    public void error(String s, Exception e) {
        logger.error(s, e);
    }
}
